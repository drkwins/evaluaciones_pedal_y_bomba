﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EvaluacionesTerrenoPyB.Migrations
{
    public partial class NumeroExpedienteComoString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Expediente",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Expediente",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
