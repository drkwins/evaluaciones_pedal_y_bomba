﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EvaluacionesTerrenoPyB.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Operador",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FechaCreado = table.Column<DateTime>(nullable: false),
                    FechaModificado = table.Column<DateTime>(nullable: false),
                    Nombres = table.Column<string>(nullable: true),
                    Apellidos = table.Column<string>(nullable: true),
                    NombreCompleto = table.Column<string>(nullable: true),
                    Codigo = table.Column<string>(nullable: true),
                    Flota = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operador", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Evaluacion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FechaCreado = table.Column<DateTime>(nullable: false),
                    FechaModificado = table.Column<DateTime>(nullable: false),
                    FechaHoraRegistro = table.Column<DateTime>(nullable: false),
                    FechaRegistro = table.Column<DateTime>(nullable: false),
                    HoraRegistro = table.Column<string>(nullable: true),
                    NombreEvaluador = table.Column<string>(nullable: true),
                    NombreOperador = table.Column<string>(nullable: true),
                    OperadorId = table.Column<int>(nullable: false),
                    Flota = table.Column<string>(nullable: true),
                    HoraLlegadaOperador = table.Column<string>(nullable: true),
                    HoraLlegadaEstimadaOperador = table.Column<string>(nullable: true),
                    Expediente = table.Column<int>(nullable: false),
                    CumpleHorarioComprometido = table.Column<string>(nullable: true),
                    MyProperty = table.Column<int>(nullable: false),
                    FacilidadComunicacion = table.Column<int>(nullable: false),
                    PresentacionPersonal = table.Column<int>(nullable: false),
                    CumpleRegistroFoto = table.Column<string>(nullable: true),
                    DelimitaAreaSeguridad = table.Column<int>(nullable: false),
                    CorrectaSujecionVh = table.Column<string>(nullable: true),
                    ImplementosSeguridad = table.Column<int>(nullable: false),
                    Observaciones = table.Column<string>(nullable: true),
                    ComunaOrigen = table.Column<string>(nullable: true),
                    ComunaDestino = table.Column<string>(nullable: true),
                    EstadoGruaRis = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Evaluacion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Evaluacion_Operador_OperadorId",
                        column: x => x.OperadorId,
                        principalTable: "Operador",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Evaluacion_OperadorId",
                table: "Evaluacion",
                column: "OperadorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Evaluacion");

            migrationBuilder.DropTable(
                name: "Operador");
        }
    }
}
