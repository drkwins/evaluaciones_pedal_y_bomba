﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EvaluacionesTerrenoPyB.Migrations
{
    public partial class OperadorIdNulable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Evaluacion_Operador_OperadorId",
                table: "Evaluacion");

            migrationBuilder.AlterColumn<int>(
                name: "OperadorId",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Evaluacion_Operador_OperadorId",
                table: "Evaluacion",
                column: "OperadorId",
                principalTable: "Operador",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Evaluacion_Operador_OperadorId",
                table: "Evaluacion");

            migrationBuilder.AlterColumn<int>(
                name: "OperadorId",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Evaluacion_Operador_OperadorId",
                table: "Evaluacion",
                column: "OperadorId",
                principalTable: "Operador",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
