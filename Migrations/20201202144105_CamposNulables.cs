﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EvaluacionesTerrenoPyB.Migrations
{
    public partial class CamposNulables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MyProperty",
                table: "Evaluacion");

            migrationBuilder.AlterColumn<int>(
                name: "PresentacionPersonal",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ImplementosSeguridad",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "FacilidadComunicacion",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "EstadoGruaRis",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "DelimitaAreaSeguridad",
                table: "Evaluacion",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PresentacionPersonal",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ImplementosSeguridad",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FacilidadComunicacion",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EstadoGruaRis",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DelimitaAreaSeguridad",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MyProperty",
                table: "Evaluacion",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
