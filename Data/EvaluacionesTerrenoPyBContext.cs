﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EvaluacionesTerrenoPyB.Controllers;

namespace EvaluacionesTerrenoPyB.Data
{
    public class EvaluacionesTerrenoPyBContext : DbContext
    {
        public EvaluacionesTerrenoPyBContext (DbContextOptions<EvaluacionesTerrenoPyBContext> options)
            : base(options)
        {
        }

        public DbSet<EvaluacionesTerrenoPyB.Controllers.Evaluacion> Evaluacion { get; set; }

        public DbSet<EvaluacionesTerrenoPyB.Controllers.Operador> Operador { get; set; }
    }
}
