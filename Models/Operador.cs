﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EvaluacionesTerrenoPyB.Controllers
{
    public class Operador : ClaseBase
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string NombreCompleto { get; set; }
        public string Codigo { get; set; }
        public string Flota { get; set; }
    }
}
