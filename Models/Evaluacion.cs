﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EvaluacionesTerrenoPyB.Controllers
{
    public class Evaluacion : ClaseBase
    {
        public DateTime FechaHoraRegistro { get; set; }
        [Column(TypeName = "Date")]
        public DateTime FechaRegistro { get; set; }
        public string HoraRegistro { get; set; }
        public string NombreEvaluador { get; set; }
        public string NombreOperador { get; set; }
        public int? OperadorId { get; set; }
        public Operador Operador { get; set; }
        public string Flota { get; set; }
        public string HoraLlegadaOperador { get; set; }
        public string HoraLlegadaEstimadaOperador { get; set; }
        public string Expediente { get; set; }
        public string CumpleHorarioComprometido { get; set; }
        public int? FacilidadComunicacion { get; set; }
        public int? PresentacionPersonal { get; set; }
        public string CumpleRegistroFoto { get; set; }
        public int? DelimitaAreaSeguridad { get; set; }
        public string CorrectaSujecionVh { get; set; }
        public int? ImplementosSeguridad { get; set; }
        public string Observaciones { get; set; }
        public string ComunaOrigen { get; set; }
        public string ComunaDestino { get; set; }
        public int? EstadoGruaRis { get; set; }
    }
}
